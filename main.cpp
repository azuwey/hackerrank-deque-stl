/**
 * Author: Zarandi David (Azuwey)
 * Date: 03/04/2019
 * Source: HackerRank - https://www.hackerrank.com/challenges/deque-stl/problem
 **/
#include <deque>
#include <iostream>
using namespace std;

void printKMax(int arr[], int n, int k) {
  deque<int> c;
  for (auto it = arr; it != arr + (n - k + 1); ++it) {
    c = deque<int>(it, it + k);
    int max = -1;
    for (auto itc = c.begin(); itc != c.end(); ++itc) {
      if (*itc > max) {
        max = *itc;
      }
    }
    cout << max << " ";
  }
  cout << endl;
}

int main() {
  int t;
  cin >> t;
  while (t > 0) {
    int n, k;
    cin >> n >> k;
    int i;
    int arr[n];
    for (i = 0; i < n; i++) cin >> arr[i];
    printKMax(arr, n, k);
    t--;
  }
  return 0;
}
